import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Blog';

  posts = [
    {
      title : 'The lords : notes on vision',
      content : 'Le cinéma ne dérive pas de la peinture, de la littérature, de la sculpture, ' +
        'du théâtre, mais d\'une ancienne et populaire tradition de sorcellerie. C\'est la manifestation contemporaine d\'une longue ' +
        'histoire d\'ombres, un ravissement de l\'image qui bouge, une croyance en la magie. Son lignage est couplé depuis sa plus ' +
        'lointaine origine avec les prêtres et la sorcellerie, une convocation des spectres. Avec, au début, l\'aide modeste du miroir et' +
        ' du feu. Les hommes ont conjuré des ombres et secrètes visites des régions enfouies de la pensée. Dans ces séances, les ombres' +
        ' sont des esprits qui éloignent le mal.',
      loveIts : 4,
      createdAt : new Date()
    },
    {
      title : 'Discours devant la chambre des communes',
      content : 'Vous vous demandez : quel est notre but ? Je réponds par un seul mot : la victoire, la victoire à n\'importe quel prix,' +
        ' la victoire en dépit de toutes les terreurs, la victoire quelque longue et difficile que soit la route pour y parvenir, car' +
        ' sans victoire, il n\'y a pas de survie.',
      loveIts : -2,
      createdAt : new Date()
    },
    {
      title : 'Moi, Delon : l’interview de sa vie',
      content : 'Il y a ces êtres que je hais. Tout est faux, tout est faussé. Il n’y a plus de respect, plus de parole donnée. Il n’y ' +
        'a que l’argent qui compte. On entend parler de crimes à longueur de journée. Je sais que je quitterai ce monde sans regrets.',
      loveIts : 0,
      createdAt : new Date()
    }
  ];

}
