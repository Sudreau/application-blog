import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {

  @Input() title: String;
  @Input() content: String;
  @Input() loveIts;
  @Input() createAt: Date;

  constructor() { }

  ngOnInit() {
  }

  onLike() {
    this.loveIts += 1;
  }
  onDislike() {
    this.loveIts -= 1;
  }

}
